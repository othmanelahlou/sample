import { all, takeLatest, put, call } from 'redux-saga/effects';
import { userSlice } from './reducer.ts';
import { openErrorToast, openSuccessToast } from '../../../core/utils/notifications.tsx';

export function* fetchUsersWorker(action: any) {
  try {

    console.log(action);
  } catch (error) {
    yield put(userSlice.actions.fetchUsersFail(error));
    yield call(openErrorToast, {title: 'Users', description: 'An error occurred while fetching users information'});
  }
}

export function* deleteUsersWorker() {
  try {

    yield call(openSuccessToast, {title: 'Users', description: 'The User was deleted successfully'});
  } catch (error) {
    yield put(userSlice.actions.deleteUserFail(error));
    yield call(openErrorToast, {title: 'Users', description: 'An error occurred while deleting users information'});
  }
}

export function* addUsersWorker(action: any) {
  try {

    yield put(userSlice.actions.addUserSuccess(action.payload));
    yield call(openSuccessToast, {title: 'Users', description: 'The User was added successfully'});
  } catch (error) {
    yield put(userSlice.actions.addUserFail(error));
    yield call(openErrorToast, {title: 'Users', description: 'An error occurred while adding users information'});
  }
}

export function* editUsersWorker() {
  try {

    yield call(openSuccessToast, {title: 'Users', description: 'The User was edited successfully'});
  } catch (error) {
    yield put(userSlice.actions.editUserFail(error));
    yield call(openErrorToast, {title: 'Users', description: 'An error occurred while editing users information'});
  }
}

export function* userSaga() {
  yield all(
    [
      takeLatest(userSlice.actions.fetchUsers, fetchUsersWorker),
      takeLatest(userSlice.actions.deleteUser, deleteUsersWorker),
      takeLatest(userSlice.actions.addUser, addUsersWorker),
      takeLatest(userSlice.actions.editUser, editUsersWorker),
    ]
  )
}