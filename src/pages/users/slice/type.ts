import { FetchStatus } from '../../../core/store/type.ts';

export interface userState {
  data: any,
  fetchStatus: FetchStatus,
  pagination: any,
  error: any
}
