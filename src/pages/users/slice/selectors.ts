import { RootState } from '../../../core/store/type.ts';
import { initialState } from './reducer.ts';
import { createSelector } from '@reduxjs/toolkit';

const selectSlice = (state: RootState) => state.users || initialState;

export const selectUserData = createSelector(
  [selectSlice],
  state => state.data,
);

export const userFetchStatus = createSelector(
  [selectSlice],
  state => state.fetchStatus,
);
