import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { FetchStatus } from '../../../core/store/type.ts';
import { userState } from './type.ts';

export const initialState: userState = {
  data: [],
  fetchStatus: FetchStatus.INITIAL,
  pagination: {},
  error: null
}

export const userSlice = createSlice({
  name: 'user',
  initialState,
  reducers: {
    fetchUsers(state, _action: PayloadAction<any>) {
      state.fetchStatus = FetchStatus.LOADING;
    },
    fetchUsersSuccess(state, action: PayloadAction<any>) {
      state.fetchStatus = FetchStatus.SUCCESS;
      state.data = action.payload;
    },
    fetchUsersFail(state, action : PayloadAction<any>) {
      state.fetchStatus = FetchStatus.ERROR;
      state.data = [];
      state.error = action.payload;
    },
    deleteUser(state, _action: PayloadAction<any>) {
      state.fetchStatus = FetchStatus.LOADING;
    },
    deleteUserSuccess(state, _action: PayloadAction<any>) {
      state.fetchStatus = FetchStatus.SUCCESS;
    },
    deleteUserFail(state, action: PayloadAction<any>) {
      state.fetchStatus = FetchStatus.ERROR;
      state.error = action.payload;
    },
    addUser(state, _action: PayloadAction<any>) {
      state.fetchStatus = FetchStatus.LOADING;
    },
    addUserSuccess(state, _action: PayloadAction<any>) {
      state.fetchStatus = FetchStatus.SUCCESS;
      state.data = [...state.data, _action.payload];
    },
    addUserFail(state, action: PayloadAction<any>) {
      state.fetchStatus = FetchStatus.ERROR;
      state.error = action.payload;
    },
    editUser(state, _action: PayloadAction<any>) {
      state.fetchStatus = FetchStatus.LOADING;
    },
    editUserSuccess(state, _action: PayloadAction<any>) {
      state.fetchStatus = FetchStatus.SUCCESS;
    },
    editUserFail(state, action: PayloadAction<any>) {
      state.fetchStatus = FetchStatus.ERROR;
      state.error = action.payload;
    }
  }
});


