import styled from 'styled-components';

export const UserHeader = styled.div`
    display: flex;
    width: 100%;
    justify-content: space-between;
    align-items: center;
`;

export const UserOptions = styled.div`
    display: flex;
    gap: 10px;
`;
