import { Table } from '../../components/table';
import { GridOption } from './type.ts';
import { UserHeader, UserOptions } from './style.ts';
import { PrimaryTitle } from '../../core/style/global.ts';
import { Button } from 'antd';
import { UserAddOutlined, UserDeleteOutlined } from '@ant-design/icons';
import { ModalComponent } from '../../components/modal';
import { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { UserForm } from '../../components/userForm';
import { userSlice } from './slice/reducer.ts';
import { UserType } from '../../components/userForm/type.ts';
import { selectUserData } from './slice/selectors.ts';

export const Users = () => {
  const [open, setOpen] = useState(false);
  const dispatch = useDispatch();

  const rowData = useSelector(selectUserData);

  const gridOptions= {
    ...GridOption,
    rowData: []
  }

  useEffect(() => {
    dispatch(userSlice.actions.fetchUsers({}));
  }, [dispatch]);

  const closeModal = () => {
    setOpen(false);
  }

  const createUser = (event: UserType) => {
    dispatch(userSlice.actions.addUser(event));
    setOpen(false);
  }

  const header = () => {
    return (
      <UserHeader>
        <PrimaryTitle>
          Users Management
        </PrimaryTitle>

        <UserOptions>
          <Button type="primary" shape="round" icon={<UserAddOutlined />} onClick={() => setOpen(true)}> New User</Button>
          <Button shape="round" icon={<UserDeleteOutlined />}>Delete Users</Button>
        </UserOptions>

        <ModalComponent open={open} title={'User Derails'} onCancel={closeModal} >
          <UserForm onFinish={createUser} onCancel={closeModal} />
        </ModalComponent>
      </UserHeader>
    )
  }

  return (
    <>
      <Table gridOptions={gridOptions} rowData={rowData} showHeader={true} tableOptions={header} />
    </>
  );
};
