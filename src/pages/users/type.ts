import { AgGridReactProps } from 'ag-grid-react';
import { UserOptionsRenderer } from '../../components/agGridComponents/userOptionsRenderer';
import { UserRoleRenderer } from '../../components/agGridComponents/userRoleRenderer';

export const GridOption: AgGridReactProps = {
  rowSelection: 'multiple',
  pagination: true,
  columnDefs: [
    {
      field: 'userId',
      headerName: 'User UI',
      filter: true,
      flex: 1,
      minWidth: 80,
      headerCheckboxSelection: true,
      checkboxSelection: true,
    },
    { field: 'name', headerName: 'Name', filter: true, flex: 1, minWidth: 120,
      valueGetter: (row) => `${row.data.firstName} ${row.data.lastName}`
    },
    { field: 'email', headerName: 'Email', filter: true, flex: 2, minWidth: 200 },
    { field: 'phoneNumber', headerName: 'Phone', filter: true, flex: 1, minWidth: 160 },
    { field: 'organization', headerName: 'Organization', filter: true, flex: 1, minWidth: 120 },
    { field: 'role', headerName: 'Role', filter: true, flex: 1, minWidth: 120, cellRenderer: UserRoleRenderer },
    { field: 'status', headerName: 'Status', filter: true, flex: 1, minWidth: 120 },
    { field: 'actions', headerName: '', filter: true, flex: 1, minWidth: 80, cellRenderer: UserOptionsRenderer},
  ]
};

export enum userRole {
  ADMIN = 'Admin',
  MANAGER = 'Manager',
  USER = 'User',
}
