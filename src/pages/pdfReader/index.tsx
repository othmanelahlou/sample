// @ts-nocheck
import { Worker } from '@react-pdf-viewer/core';
import { ScrollMode, SpecialZoomLevel, Viewer } from '@react-pdf-viewer/core';
import { RenderGoToPageProps } from '@react-pdf-viewer/page-navigation';
import { toolbarPlugin, ToolbarSlot } from '@react-pdf-viewer/toolbar';

// Import styles
import '@react-pdf-viewer/page-navigation/lib/styles/index.css';
import '@react-pdf-viewer/core/lib/styles/index.css';

import pdfFile from '../../assets/pdf/example.pdf';
import { DndProvider } from 'react-dnd';
import { HTML5Backend } from 'react-dnd-html5-backend';
import { ImageList } from '../../components/image-list';
import { CanvasDropArea } from '../../components/canvas-drop-area';

export function PdfReader() {
  const toolbarPluginInstance = toolbarPlugin();
  const { Toolbar } = toolbarPluginInstance;

  return (
    <Worker workerUrl="https://unpkg.com/pdfjs-dist@2.6.347/build/pdf.worker.min.js">
      <DndProvider backend={HTML5Backend}>
        <ImageList />

        <div
          style={{
            border: '1px solid rgba(0, 0, 0, 0.3)',
            position: 'relative',
            height: '750px',
            width: '550px',
            overflow: 'hidden',
          }}
        >
          <CanvasDropArea />

          <Toolbar>
            {(props: ToolbarSlot) => {
              const {
                CurrentPageInput,
                GoToNextPage,
                GoToPreviousPage,
                NumberOfPages,
              } = props;
              return (
                <div style={{ display: 'flex', alignItems: 'center' }}>
                  <div style={{ padding: '0px 2px', marginLeft: 'auto' }}>
                    <GoToPreviousPage>
                      {(props: RenderGoToPageProps) => (
                        <button
                          style={{
                            backgroundColor: props.isDisabled
                              ? '#96ccff'
                              : '#357edd',
                            border: 'none',
                            borderRadius: '4px',
                            color: '#ffffff',
                            cursor: props.isDisabled ? 'not-allowed' : 'pointer',
                            padding: '8px',
                          }}
                          disabled={props.isDisabled}
                          onClick={props.onClick}
                        >
                          Previous page
                        </button>
                      )}
                    </GoToPreviousPage>
                  </div>
                  <div style={{ padding: '0px 2px', width: '4rem' }}>
                    <CurrentPageInput />
                  </div>
                  <div style={{ padding: '0px 2px' }}>
                    / <NumberOfPages />
                  </div>
                  <div style={{ padding: '0px 2px' }}>
                    <GoToNextPage>
                      {(props: RenderGoToPageProps) => (
                        <button
                          style={{
                            backgroundColor: props.isDisabled
                              ? '#96ccff'
                              : '#357edd',
                            border: 'none',
                            borderRadius: '4px',
                            color: '#ffffff',
                            cursor: props.isDisabled ? 'not-allowed' : 'pointer',
                            padding: '8px',
                          }}
                          disabled={props.isDisabled}
                          onClick={props.onClick}
                        >
                          Next page
                        </button>
                      )}
                    </GoToNextPage>
                  </div>
                </div>
              );
            }}
          </Toolbar>

          <Viewer
            fileUrl={pdfFile}
            scrollMode={ScrollMode.Page}
            defaultScale={SpecialZoomLevel.PageFit}
            plugins={[toolbarPluginInstance]}
          />
        </div>
      </DndProvider>
    </Worker>
  );
}


