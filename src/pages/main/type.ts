export enum Pages {
  APPROVAL_TASKS = 'approvalTasks',
  DASHBOARD = 'Table',
  PDF_READER = 'pdfReader',
  E_SIGN_REQUEST = 'eSignRequest',
  E_SIGN_API = 'eSignAPI',
  E_SIGN_TASKS = 'eSignTasks',
  USERS = 'users',
  CONTACTS = 'contacts'
}
