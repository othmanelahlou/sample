import styled from 'styled-components';

export const AppWrapper = styled.div`
    display: flex;
    width: 100vw;
    height: 100vh;
`;

export const customMenuStyle = {
  maxWidth: '200px'
};
