
import { PdfReader } from '../pdfReader';
import { Menu } from 'antd';
import { useState } from 'react';
import type { MenuProps } from 'antd';
import {
  FilePdfOutlined,
  FileTextOutlined,
  MenuFoldOutlined,
  MenuUnfoldOutlined,
  PieChartOutlined,
} from '@ant-design/icons';
import { AppWrapper, customMenuStyle } from './style.ts';
import { Footer } from '../../components/footer';
import { Pages } from './type.ts';

import { ApprouvalTasks } from '../approvalTasks';
import { ESignApi } from '../eSignApi';
import { Dashboard } from '../dashboard';
import { Users } from '../users';
import { ESignRequest } from '../eSignRequest';
import { ESignTasks } from '../eSignTasks';
import { Contacts } from '../contacts';

type MenuItem = Required<MenuProps>['items'][number];

export function Main() {
  const [collapsed, setCollapsed] = useState(false);
  const [selectedPage, setSelectedPage] = useState(Pages.DASHBOARD);

  const items: MenuItem[] = [
    {
      key: '1', icon: collapsed ? <MenuUnfoldOutlined /> : <MenuFoldOutlined />, label: '', onClick: () => {
        setCollapsed(!collapsed);
      },
    },
    {
      key: '2', icon: <PieChartOutlined />, label: 'Dashboard', onClick: () => {
        setSelectedPage(Pages.DASHBOARD);
      },
    },
    {
      key: '3', icon: <span className="material-symbols-outlined">stacks</span>, label: 'eSign Requests', onClick: () => {
        setSelectedPage(Pages.E_SIGN_REQUEST);
      },
    },
    {
      key: '4', icon: <FileTextOutlined />, label: 'eSignTasks', onClick: () => {
        setSelectedPage(Pages.E_SIGN_TASKS);
      },
    },
    {
      key: '5', icon: <span className="material-symbols-outlined">contract_edit</span>, label: 'Approval Tasks', onClick: () => {
        setSelectedPage(Pages.APPROVAL_TASKS);
      },
    },
    {
      key: '6', icon: <FilePdfOutlined />, label: 'My Documents', onClick: () => {
        setSelectedPage(Pages.PDF_READER);
      },
    },
    {
      key: '7', icon: <span className="material-symbols-outlined">groups</span>, label: 'Users', onClick: () => {
        setSelectedPage(Pages.USERS);
      },
    },
    {
      key: '8', icon: <span className="material-symbols-outlined">recent_actors</span>, label: 'Contacts', onClick: () => {
        setSelectedPage(Pages.CONTACTS);
      },
    },
    {
      key: '9', icon: <span className="material-symbols-outlined">code</span>, label: 'eSign Api', onClick: () => {
        setSelectedPage(Pages.E_SIGN_API);
      },
    },
  ];

  const returnedSegment = () => {
    switch (selectedPage) {
      case Pages.DASHBOARD :
        return <Dashboard />;
      case Pages.PDF_READER:
        return <PdfReader />;
      case Pages.APPROVAL_TASKS:
        return <ApprouvalTasks />;
      case Pages.E_SIGN_API:
        return <ESignApi />;
      case Pages.USERS:
        return <Users />
      case Pages.E_SIGN_REQUEST:
        return <ESignRequest />
      case Pages.E_SIGN_TASKS:
        return <ESignTasks />
      case Pages.CONTACTS:
        return <Contacts />
    }
  };

  return (
    <AppWrapper>
      <Menu
        mode="inline"
        theme="dark"
        inlineCollapsed={collapsed}
        items={items}
        style={customMenuStyle}
      />
      {returnedSegment()}
      <Footer />
    </AppWrapper>
  );
}