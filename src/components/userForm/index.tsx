import { Button, Form, Input, Radio, Select, Space } from 'antd';
import { initialUserForm, UserFormInterface, UserType } from './type.ts';
import { userRole } from '../../pages/users/type.ts';
import { UserOptions } from '../../pages/users/style.ts';
const { Option } = Select;

export const UserForm = ({onFinish, onFinishFailed, onCancel}: UserFormInterface) => {
  const prefixSelector = (
    <Form.Item name="prefix" noStyle>
      <Select style={{ width: 100 }}>
        <Option value="212">+212</Option>
        <Option value="87">+87</Option>
      </Select>
    </Form.Item>
  );

  return (
    <>
      <Form
        name="userForm"
        labelCol={{ span: 8 }}
        wrapperCol={{ span: 16 }}
        style={{ maxWidth: 600 }}
        initialValues={initialUserForm}
        onFinish={onFinish}
        onFinishFailed={onFinishFailed}
        autoComplete="off"
      >
        <Form.Item<UserType>
          label="Firstname"
          name="firstName"
          rules={[{ required: true, message: 'Please input a valid firstname!' }]}
        >
          <Input />
        </Form.Item>

        <Form.Item<UserType>
          label="LastName"
          name="lastName"
          rules={[{ required: true, message: 'Please input a valid lastName!' }]}
        >
          <Input />
        </Form.Item>

        <Form.Item<UserType>
          label="Email"
          name="email"
          rules={[{ required: true, type: 'email', message: 'Please input a valid email!' }]}
        >
          <Input />
        </Form.Item>

        <Form.Item<UserType>
          name="phoneNumber"
          label="Phone Number"
          rules={[{ required: true, message: 'Please input your phone number!' }]}
        >
          <Input addonBefore={prefixSelector} style={{ width: '100%' }} />
        </Form.Item>

        <Form.Item<UserType> label="Organization" name="organization">
          <Select>
            <Select.Option value="Digeleo">DIGELEO</Select.Option>
            <Select.Option value="React">REACT</Select.Option>
          </Select>
        </Form.Item>

        <Form.Item<UserType> label="Role" name="role">
          <Radio.Group >
            <Space direction="vertical" style={{paddingTop: 6}}>
              <Radio value={userRole.ADMIN}>{userRole.ADMIN}</Radio>
              <Radio value={userRole.MANAGER}>{userRole.MANAGER}</Radio>
              <Radio value={userRole.USER}>{userRole.USER}</Radio>
            </Space>
          </Radio.Group>
        </Form.Item>

        <UserOptions style={{justifyContent: 'end'}}>
          <Button type="primary" htmlType="submit">Confirm</Button>
          <Button onClick={onCancel}>Cancel</Button>
        </UserOptions>
      </Form>
    </>
  )
}