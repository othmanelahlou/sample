import { userRole } from '../../pages/users/type.ts';

export interface UserFormInterface {
  onFinish: any,
  onFinishFailed?: any,
  onCancel?: any
}

export type UserType = {
  firstName: string;
  lastName: string;
  email: string;
  phoneNumber: string;
  organization: any;
  role: string;
  prefix: string;
};

export const initialUserForm = {
  firstName: '',
  lastName: '',
  email: '',
  phoneNumber: '',
  organization: '',
  role: userRole.ADMIN,
  prefix: '212',
}
