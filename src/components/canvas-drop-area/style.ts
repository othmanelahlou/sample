import styled from 'styled-components';

export const DraggableOverlay = styled.div`
    width: 524px;
    height: 682px;
    position: absolute;
    top: 65px;
    background: transparent;
    right: 12px;
    z-index: 100;
`;
