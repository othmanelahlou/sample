// @ts-nocheck
import { useState } from 'react';
import { useDrop } from 'react-dnd';
import { DragItem } from '../drag-item';
import { DraggableOverlay } from './style.ts';

export const CanvasDropArea = () => {
  const [droppedImages, setDroppedImages]: any = useState([]);
  const [{ isOver, canDrop }, drop] = useDrop({
    accept: 'image',
    drop: (item: any, monitor) => {
      const { x, y }: any = monitor.getSourceClientOffset();
      let div = document.getElementById('droppable');
      let dropTargetXy: any = div?.getBoundingClientRect();

      setDroppedImages(prevImages => {
        const addedIndex = prevImages.findIndex(
          image => image?.id === item?.id,
        );
        return addedIndex > -1
          ? prevImages.map((image, index) =>
            addedIndex === index
              ? {
                ...item,
                x: x - dropTargetXy?.left,
                y: y - dropTargetXy?.top,
              }
              : image,
          )
          : [
            ...prevImages,
            { ...item, x: x - dropTargetXy?.left, y: y - dropTargetXy?.top },
          ];
      });
    },
    collect: monitor => ({
      isOver: monitor.isOver(),
      canDrop: monitor.canDrop(),
    }),
  });

  return (
    <DraggableOverlay ref={drop} id="droppable">
      {droppedImages.map(image => (
        <DragItem
          key={image.id}
          image={image}
          style={{
            position: 'absolute',
            zIndex: 300,
            left: image.x,
            top: image.y,
          }}
        />
      ))}
    </DraggableOverlay>
  );
};

