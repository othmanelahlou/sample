import { Modal } from 'antd';
import React from 'react';

export interface ModalProps extends React.HTMLProps<any> {
  open : boolean;
  title : string;
  onConfirm ?: any;
  onCancel : any;
  footer ?: any;
}

export const ModalComponent = ({open, title, onConfirm, onCancel, footer = <></>, children }: ModalProps) => {
  return (
    <>
      <Modal
        title={title}
        open={open}
        onCancel={onCancel}
        onOk={onConfirm}
        footer={footer}
      >
        {children}
      </Modal>
    </>
  )
};
