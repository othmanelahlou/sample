// @ts-nocheck
import { BuildVersion } from './style.ts';
import packageJson from '../../../package.json';
import { Typography } from 'antd';

const { Text } = Typography;

export function Footer() {

  return(
    <BuildVersion>
      <span className="roboto-light">
        {`Build Version ${packageJson.version}`}
      </span>
    </BuildVersion>
  )
}
