import styled from 'styled-components';

export const BuildVersion = styled.div`
    background: white;
    width: 100vw;
    display: flex;
    justify-content: flex-end;
    position: absolute;
    bottom: 0;
    border-top: 1px solid rgba(0, 0, 0, 0.15);
    
    span {
        padding-right: 15px;
    }
`;