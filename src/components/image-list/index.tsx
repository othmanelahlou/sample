import { DragItem } from '../drag-item';
import { ListItems } from './style.ts';

export const ImageList = () => {
  const images = [
    { id: 1, src: 'src/assets/images/signature1.png', text: 'text1' },
    { id: 2, src: 'src/assets/images/signature1.png', text: 'text2' },
  ];

  return (
    <ListItems>
      {images.map(image => (
        <DragItem key={image.id} image={image} style={{}} />
      ))}
    </ListItems>
  )
}