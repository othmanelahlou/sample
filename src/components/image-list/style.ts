import styled from 'styled-components';

export const ListItems = styled.div`
    height: 750px;
    margin: 0 10px;
    min-width: 100px;
    padding: 0 10px;
    
    div {
        margin: 10px 0;
    }
`;
