import { CustomCellRendererProps } from 'ag-grid-react';
import { Tag } from 'antd';

export const UserRoleRenderer = (props: CustomCellRendererProps) => {
  return (
    <>
      <Tag color="#2db7f5">{props.data?.role}</Tag>
    </>
  );
};