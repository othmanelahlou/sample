import { CustomCellRendererProps } from 'ag-grid-react';
import { IconsWrapper } from '../../../core/style/global.ts';
import { Tooltip } from 'antd';

export const UserOptionsRenderer = (props: CustomCellRendererProps) => {
  return (
    <IconsWrapper>
      <Tooltip placement="top" title={'Edit User'}>
        <span className="material-symbols-outlined">person_edit</span>
      </Tooltip>

      <Tooltip placement="top" title={props.data?.locked ? 'lock User' : 'Unlock User'}>
        {
          props.data?.locked ? <span className="material-symbols-outlined">lock_open</span> :
            <span className="material-symbols-outlined">lock</span>
        }
      </Tooltip>
    </IconsWrapper>
  );
};