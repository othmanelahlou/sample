// @ts-nocheck
import { useDrag } from 'react-dnd';

export function DragItem({ image, style }) {
  const [{ isDragging }, drag] = useDrag({
    type: 'image',
    item: image,
    collect: monitor => ({
      isDragging: monitor.isDragging(),
    }),
  });

  return (
    <div ref={drag} style={{ ...style, cursor: 'move', width: 'fit-content' }}>
      {image.text}
      <img
        src={image.src}
        alt={`${image.id}`}
        style={{ width: '60px', height: '50px' }}
      />
    </div>
  );
}