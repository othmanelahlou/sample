// @ts-nocheck
import { AgGridReact } from 'ag-grid-react';
import { TableHeader, TableWrapper } from './style.ts';

export function Table({gridOptions, rowData, showHeader = false, tableOptions = () => <></>}) {

  return (
    <TableWrapper>
      {showHeader && <TableHeader>
        {tableOptions()}
      </TableHeader>}

      <div className="ag-theme-quartz" style={{ width: '100%', height: '100%' }}>
        <AgGridReact {...gridOptions} rowData={rowData} />
      </div>
    </TableWrapper>

  );
}
