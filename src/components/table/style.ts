import styled from 'styled-components';

export const TableWrapper = styled.div`
    display: flex;
    flex-direction: column;
    width: 100%;
    padding: 10px 15px 50px;
`;

export const TableHeader = styled.div`
    display: flex;
    width: 100%;
    padding: 10px 0;
    margin-bottom: 10px;
`;
