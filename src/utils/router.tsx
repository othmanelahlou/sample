import {createBrowserRouter} from "react-router-dom";
import Index from "../pages/login";
import { Main } from '../pages/main';

export const router = createBrowserRouter([
    {
        path: '/',
        element: <Index />,
    },
    {
        path: '/home',
        element: <Main />
    }
])