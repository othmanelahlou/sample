import React from 'react';
import ReactDOM from 'react-dom/client';
import './index.css';
import CoreApp from './core';


ReactDOM.createRoot(document.getElementById('root')!).render(
  <React.StrictMode>
      <CoreApp />
  </React.StrictMode>,
)
