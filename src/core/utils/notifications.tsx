import { toast } from 'react-toastify';
import { ToastLayout } from '../style/global.ts';

export type NotificationType = 'success' | 'info' | 'warning' | 'error';

interface ToastInterface {
  title: string,
  description: string
}

const defaultToastTemplate = (title: string, description: string) => {
  return (
    <ToastLayout>
      <span className="roboto-bold">{title}</span>
      <span className="roboto-light">{description}</span>
    </ToastLayout>
  );
};

export const openSuccessToast = ({ title, description }: ToastInterface) => {
  toast.success(
    defaultToastTemplate(title, description), {
      position: 'bottom-right',
      autoClose: 3000,
    });
};

export const openErrorToast = ({ title, description }: ToastInterface) => {
  toast.error(
    defaultToastTemplate(title, description), {
      position: 'bottom-right',
      autoClose: 3000,
    });
};

export const openInfoToast = ({ title, description }: ToastInterface) => {
  toast.info(
    defaultToastTemplate(title, description), {
      position: 'bottom-right',
      autoClose: 3000,
    });
};

