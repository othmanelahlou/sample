import { configureAppStore } from './configureStore.ts';

export const store = configureAppStore()
