import {combineReducers} from "@reduxjs/toolkit";
import { userSlice } from '../../pages/users/slice/reducer.ts';

export const reducers = combineReducers({
  users: userSlice.reducer
});
