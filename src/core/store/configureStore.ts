import { configureStore } from '@reduxjs/toolkit';
import createSagaMiddleware from 'redux-saga';
import { reducers } from './reducers.ts';
import rootSaga from './rootSaga.ts';

export function configureAppStore() {

    const sagaMiddleware = createSagaMiddleware({});
    const {run: runSaga} = sagaMiddleware;

    const middlewares = [sagaMiddleware];

    const store = configureStore({
        reducer: reducers,
        middleware: (getDefaultMiddleware) => {
            return getDefaultMiddleware({}).concat([...middlewares])
        },
        devTools: true
    });

    runSaga(rootSaga);

    return store
}