import { all } from 'redux-saga/effects';
import { userSaga } from '../../pages/users/slice/saga.ts';

export default function* rootSaga() {
  yield all([
    userSaga(),
  ])
}