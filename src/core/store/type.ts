import { userState } from '../../pages/users/slice/type.ts';

export enum FetchStatus {
  INITIAL = 'initial',
  PENDING = 'pending',
  SUCCESS = 'success',
  ERROR = 'error',
  LOADING = 'loading'
}

export interface RootState {
  users: userState
}
