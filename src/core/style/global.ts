import styled from 'styled-components';

export const PrimaryTitle = styled.div`
    font-weight: 600;
    font-size: 18px;
    color: rgba(0, 0, 0, 0.6);
    white-space: nowrap;
`;

export const IconsWrapper = styled.div`
    gap: 5px;
    display: flex;
    align-items: center;
    height: 100%;
    span {
        cursor: pointer;
        font-size: 22px;
    }
`;

export const ToastLayout = styled.div`
    display: flex;
    flex-direction: column;
    
    .roboto-bold { 
        font-size: 15px;
    }
    
    .roboto-light { 
        font-size: 13px;
    }
`;
